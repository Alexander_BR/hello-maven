package at.hakwt;
import com.google.common.math.BigIntegerMath;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.JavaVersion;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigInteger;

public class Main {
    public static void main(String[] args) throws IOException {
        if ( ! at.hakwt.VersionCheck.isValidJavaVersion() )
        {
            System.out.println("Du bist auf der falschen Java Version: " + VersionCheck.getCurrentJavaVersion());
            System.exit(-1);
        }
        System.out.println("Hello world!");
        System.out.println(JavaVersion.JAVA_17);
        Reader reader = new FileReader("kundendaten.csv");
        CSVParser parser = CSVFormat.newFormat(';' ).parse(reader);

        for (CSVRecord r : parser.getRecords()) {
            StringBuilder sb = new StringBuilder();
            sb.append(r.get(0) + " | ");
            sb.append(r.get(1) + " | ");
            sb.append(r.get(2) + " | ");
            sb.append(r.get(3) + " | ");
            sb.append(r.get(4) + " | ");
            sb.append(r.get(5) + " | ");
            sb.append(r.get(6) + " | ");

            System.out.println(sb);

            System.out.print(r.get(0) + " | ");
            System.out.print(r.get(1) + " | ");
            System.out.print(r.get(2) + " | ");
            System.out.print(r.get(3) + " | ");
            System.out.print(r.get(4) + " | ");
            System.out.print(r.get(5) + " | ");
            System.out.print(r.get(6) + " | ");

            System.out.println("");
        }
        for (int i = 1; i < 10; i++)
        {
            BigInteger ans1 = BigIntegerMath.factorial(i);
            System.out.println("Fakultät von " +i+" ist: " + ans1);
        }
    }
}