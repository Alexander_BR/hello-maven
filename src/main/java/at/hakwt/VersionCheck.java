package at.hakwt;

import org.apache.commons.lang3.SystemUtils;
public class VersionCheck {

    public static boolean isValidJavaVersion(){
        return getCurrentJavaVersion().equals(getMinJavaVersion());
    }

    public static String getCurrentJavaVersion(){
        return SystemUtils.getJavaHome().getName();
    }

    public static String getMinJavaVersion(){
        return "openjdk-17";
    }

}
